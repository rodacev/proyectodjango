from django.db import models

# Create your models here.

# AQUI CREO EL MODELO DE LA BASE DE DATOS, PARA EL EJEMPLO UNA SOLA CLASE

class Articulos(models.Model):

    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=20)
    precio = models.IntegerField()