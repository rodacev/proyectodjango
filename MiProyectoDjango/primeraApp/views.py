from django.http import HttpResponse
import datetime
from django.template import Template, Context
from django.template import loader
from django.shortcuts import render


def saludo(request):

    nombre = "Rodrigo"
    apellido = "Acevedo"

    documento = loader.get_template("base.html")

    doc = documento.render({"nombre_persona": nombre, "apellido_persona": apellido})

    return HttpResponse(doc)
